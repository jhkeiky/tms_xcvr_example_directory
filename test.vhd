library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test is
generic
(
WIDTH : natural := 8
);
port 
( 
  CLK     : in    std_logic;
  RST     : in    std_logic := '0';
  DATAI   : in 	  std_logic_vector(WIDTH-1 downto 0);
  DATAO   : out   std_logic_vector(WIDTH-1 downto 0) := (others => '0')
 );
end test;

architecture rtl of test is

begin
  
  process(CLK,RST)
  begin
    if RST='1' then
		DATAO <= (others => '0');
    elsif rising_edge(CLK) then
	    DATAO <= DATAI;  

    end if;
  end process;
    
end rtl;
