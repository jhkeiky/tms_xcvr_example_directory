library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity patterndetect_OR is
port
	(
		datain : in std_logic_vector(7 downto 0);
		detect : out std_logic;
		error_flag : out std_logic
	);
end patterndetect_OR;
	
architecture arch of patterndetect_OR is
begin
	detect <= '0' when datain = x"00" else '1';
	
	process(datain)
	begin
		case datain is
			when "10000000" =>
				error_flag <= '0';
			when "01000000" =>
				error_flag <= '0';
			when "00100000" =>
				error_flag <= '0';
			when "00010000" =>
				error_flag <= '0';
			when "00001000" =>
				error_flag <= '0';
			when "00000100" =>
				error_flag <= '0';
			when "00000010" =>
				error_flag <= '0';
			when "00000001" =>
				error_flag <= '0';
			when "00000000" =>
				error_flag <= '0';
			when others =>
				error_flag <= '1';
		end case;
	end process;
end;
