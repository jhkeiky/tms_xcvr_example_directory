library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bus_latch is
port 
( 
	rst : in std_logic;
	clk : in std_logic;

	datain : in std_logic_vector(7 downto 0);
	dataout: out std_logic_vector(7 downto 0) := (others => '0')
);
end bus_latch;

architecture rtl of bus_latch is
begin
	process(rst, clk)
	begin
		if rst='1' then
			dataout <= (others => '0');
		elsif rising_edge(clk) then
			dataout <= datain;
		end if;
	end process;
end rtl;