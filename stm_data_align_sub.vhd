--=========================================================================
--  This document is the property of KEIKY. The document (or any         --
--  part of it) must not be copied without permission from KEIKY.        --
--  Any authorised copies of this document must include this header.     --
--                (c) Copyright KEIKY Ltd 2019                           --
--=========================================================================
-- File        : stm_data_align_sub.vhd
--  
-- Author      : Nicholas St. Hill
-- 
--==========================================================================
-- Notes :
--
-- rx_word_aligner_rknumber - Specifies the number of valid word alignment
-- patterns that must be received before the word aligner achieves synchronization
-- lock. The default is 3.
-- 
-- rx_word_aligner_renumber - Specifies the number of invalid data codes or
-- disparity errors that must be received before the word aligner loses synchronization.
-- The default is 3
--
-- rx_word_aligner_rgnumber - 
-- Specifies the number of valid data codes that
-- must be received to decrement the error counter.
-- If the word aligner receives enough valid data
-- codes to decrement the error count to 0, the
-- word aligner returns to synchronization lock.
--==========================================================================
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity stm_data_align_sub is
generic (
		stm  : stm_type := stm1;
		rx_word_aligner_rknumber    : natural range 1 to 256 := 3;
		rx_word_aligner_renumber    : natural range 1 to 256  := 3;
		rx_word_aligner_rgnumber    : natural range 1 to 256  := 3
);
port 
( 
  reset         		: in    std_logic;
  rx_clkin      		: in    std_logic;
  rx_datain     		: in    std_logic_vector(7 downto 0);
  rx_dataout    		: out   std_logic_vector(7 downto 0);
  rx_patterndetect 		: out std_logic;
  rx_syncstatus     	: out std_logic;
  rx_pattern_a1a2 		: out std_logic;
  rx_pattern_a1 		: out std_logic;
  rx_pattern_a2 		: out std_logic
);
end stm_data_align_sub;

architecture rtl of stm_data_align_sub is
 constant STS : natural := conv(stm);
 
 constant A1 : std_logic_vector(STS_FRAME_ALIGNMENT_BYTE_A1'range) := STS_FRAME_ALIGNMENT_BYTE_A1;
 constant A2 : std_logic_vector(STS_FRAME_ALIGNMENT_BYTE_A2'range) := STS_FRAME_ALIGNMENT_BYTE_A2;

 constant A1A2 : std_logic_vector(A1'high + A1'length downto 0) := A1 & A2;

 signal word_buf : std_logic_vector(A1A2'range) := (others => '0');

 type T_STATE is (IDLE, S_A1, S_A2);
 signal state   : T_STATE := IDLE;
 
 signal cnt : natural := 0;
 
begin

rx_pattern_a1a2 <= '1' when word_buf = A1A2 else '0';

rx_pattern_a1 <= '1' when rx_datain = A1 else '0';
rx_pattern_a2 <= '1' when rx_datain = A2 else '0';

rx_syncstatus <= '0';

rx_dataout <= rx_datain;
		
process(reset, rx_clkin)

begin
	if reset='1' then
		word_buf <= (others => '0');
		state <= IDLE;
		cnt <= STS;

	elsif rising_edge(rx_clkin) then

	  word_buf <= word_buf(word_buf'high - rx_datain'length downto 0) & rx_datain;	
	  
	 if (state = S_A2) and ( cnt=0 ) then
		rx_patterndetect <= '1';
	 else
		rx_patterndetect <= '0';
	 end if;
	  
	  case state is 
		when IDLE => 			

			if rx_datain = A1 then
				state <= S_A1;
				cnt <= STS - 1;
			end if;
			
		when S_A1 => 

			if cnt = 0 then
				-- test for A2
				if rx_datain = A2 then
					state <= S_A2;
					cnt <= STS - 1; -- reset A2 count
				elsif rx_datain = A1 then
				   -- stay in this state
					cnt <= STS - 1; -- reset A1 count
				else
					state <= IDLE;
				end if;
			else
				-- test for A1
				if rx_datain = A1 then
					-- stay in the state
					cnt <= cnt - 1;
				else
					state <= IDLE;
				end if;
			end if;
				
		when S_A2 => 
			if cnt = 0 then
				-- test for A1
				if rx_datain = A1 then
					state <= S_A1;
					cnt <= STS - 1; -- Reset A1 count
				else
					state <= IDLE;
				end if;
			else
				-- test for A2
				if rx_datain = A2 then
					-- stay in the state
					cnt <= cnt - 1;
				elsif rx_datain = A1 then
					state <= S_A1;
					cnt <= STS - 1; -- reset A1 count
				else
					state <= IDLE;
				end if;
			end if;

	  end case;		

	end if;

end process;


end rtl;
