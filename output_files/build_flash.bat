@echo off
set QUARTUS_BIN="C:\intelFPGA_lite\18.1\nios2eds\bin"
set TARGET=xcvr_loopback_top

rem set CMDSTR=%QUARTUS_BIN%\sof2flash.exe
set CMDSTR=sof2flash.exe

set CMDSTR=%CMDSTR% --input=%TARGET%.sof --output=%TARGET%.flash
set CMDSTR=%CMDSTR% --offset=0xC80000
set CMDSTR=%CMDSTR% --pfl --optionbit=0x00018000 --programmingmode=FPP

@echo on
call %CMDSTR%