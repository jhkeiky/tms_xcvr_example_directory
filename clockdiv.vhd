library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clockdiv is
generic
(
N : natural := 1 -- Sets the divider by 2**N
);
port 
( 
  CLK     : in    std_logic;
  RST     : in    std_logic := '0';
  OCLK    : out   std_logic;
  OCLKn   : out   std_logic
);
end clockdiv;

architecture rtl of clockdiv is
signal cnt : unsigned(N downto 0) := (others => '0');
signal y : std_logic := '0';
begin

	OCLK <= y;
	OCLKn <= not y;
  
  process(CLK,RST)
  begin
    if RST='1' then
		cnt <= (others => '0');
		y <= '0';
    elsif rising_edge(CLK) then
	    cnt <= cnt + 1; 
		
		if cnt = 0 then
			y <= not y;
		end if;
    end if;
  end process;
    
end rtl;
