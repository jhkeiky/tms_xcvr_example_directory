library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity stm_data_align_top is
generic
	(
		stm  : stm_type := stm1;
		rx_word_aligner_rknumber    : natural range 1 to 256 := 3;
		rx_word_aligner_renumber    : natural range 1 to 256  := 3;
		rx_word_aligner_rgnumber    : natural range 1 to 256  := 3;
		rx_run_length_val           : natural range 7 to 63 := 31;		
		clkdiv_parameter            : natural := 1 -- Sets the divider by 2**N
);
port
	(
		rst : in std_logic := '0';
		clk : in std_logic;
		
		-- XCVR parallel in
		rx_datain : in  std_logic_vector(7 downto 0);
		
		-- selects the A1A2 pattern to detect
		sel: in  std_logic_vector(1 downto 0) := (others => '0'); 
		
		-- Pattern detection status outputs
		detect : out std_logic;
		detect_div : out std_logic;
		error_flag : out std_logic
	);
end stm_data_align_top;


architecture rtl of stm_data_align_top is

	signal patterndetect_bus : std_logic_vector(7 downto 0);
	signal detect_line : std_logic := '0';

begin
	detect <= detect_line;

	-- Data align wrapper subcomponent
	da_inst : entity work.stm_data_align_wrapper
	generic map (
			rx_word_aligner_rknumber => rx_word_aligner_rknumber,
			rx_word_aligner_renumber => rx_word_aligner_rknumber,
			rx_word_aligner_rgnumber => rx_word_aligner_rknumber
			)
	port map (
			reset => rst,
			rx_clkin => clk,
			rx_datain => rx_datain,
			rx_patterndetect => patterndetect_bus,
			sel => sel
			);
	
	-- Pattern Detect subcomponent
	pd_out_inst : entity work.patterndetect_OR
	port map (
			datain => patterndetect_bus,
			detect => detect_line,
			error_flag => error_flag
			);
			
	-- clock div
	clkdiv_inst : entity work.clockdiv
	generic map (N => clkdiv_parameter)
	port map (
			CLK => detect_line,
			RST => rst,
			OCLK => detect_div,
			OCLKn => open
			);


end;