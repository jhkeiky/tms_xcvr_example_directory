
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity stm_data_align_wrapper is
generic (
		stm  : stm_type := stm1;
		rx_word_aligner_rknumber    : natural range 1 to 256 := 3;
		rx_word_aligner_renumber    : natural range 1 to 256  := 3;
		rx_word_aligner_rgnumber    : natural range 1 to 256  := 3;
		rx_run_length_val           : natural range 7 to 63 := 31
);
port 
( 
  reset         		: in  std_logic;
  rx_clkin      		: in  std_logic;
  rx_datain     		: in  std_logic_vector(7 downto 0);
  rx_patterndetect 		: out std_logic_vector(7 downto 0);
  sel					: in  std_logic_vector(1 downto 0) := (others => '0')	
);
end stm_data_align_wrapper;

architecture rtl of stm_data_align_wrapper is

constant WIDTH : natural := 2 * rx_datain'length;
signal word_buf : std_logic_vector(WIDTH-1 downto 0) := (others => '0');

signal  patterndetect     	: std_logic_vector(rx_datain'range);
signal  syncstatus     	: std_logic_vector(rx_datain'range);
signal  pattern_a1a2 		: std_logic_vector(rx_datain'range);
signal  pattern_a1 		: std_logic_vector(rx_datain'range);
signal  pattern_a2 		: std_logic_vector(rx_datain'range);

begin
	rx_patterndetect <= patterndetect when sel="00" else 
						pattern_a1a2  when sel="01" else			
						pattern_a1    when sel="10" else			
						pattern_a2;		


	G_SUB : for i in rx_datain'range generate
		constant N1 : natural := i;
		constant N2 : natural := N1 + rx_datain'high;
	begin
		sub_i : entity work.stm_data_align_sub
		generic map (
				stm  => stm,
				rx_word_aligner_rknumber => rx_word_aligner_rknumber,
				rx_word_aligner_renumber => rx_word_aligner_renumber,
				rx_word_aligner_rgnumber => rx_word_aligner_rgnumber
		)
		port map
		( 
		  reset         		=> reset,
		  rx_clkin      		=> rx_clkin,
		  rx_datain     		=> word_buf(N2 downto N1),
		  rx_dataout    		=> open,
		  rx_patterndetect 		=> patterndetect(i),
		  rx_syncstatus     	=> syncstatus(i),
		  rx_pattern_a1a2 		=> pattern_a1a2(i),
		  rx_pattern_a1 		=> pattern_a1(i),
		  rx_pattern_a2 		=> pattern_a2(i)
		);
	end generate;

p_main : process(reset, rx_clkin)
begin

	if reset='1' then
		word_buf <= (others => '0');
	elsif rising_edge(rx_clkin) then
		word_buf <= word_buf(word_buf'high - rx_datain'length downto 0) & rx_datain;	
			end if;
end process;

end rtl;
