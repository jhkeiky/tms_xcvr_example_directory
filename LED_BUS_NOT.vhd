library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LED_BUS_NOT is
port
	(
		datain : in std_logic_vector(7 downto 0);
		dataout: out std_logic_vector(7 downto 0)
	);
end LED_BUS_NOT;

architecture rtl of LED_BUS_NOT is
begin
		dataout <= not datain;
end rtl;
				