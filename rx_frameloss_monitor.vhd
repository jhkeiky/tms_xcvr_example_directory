library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rx_frameloss_monitor is
generic
	(
		update_period_ms : integer := 1000;
		clk_freq_mhz : integer := 100;
		frame_count_expected : integer := 2430
);
port
	(
		rst : in std_logic;
		clk : in std_logic;
		fp : in std_logic;
		dataout : out std_logic_vector(2 downto 0);
		error_flag : out std_logic
	);
end rx_frameloss_monitor;

architecture rtl of rx_frameloss_monitor is
constant ERROR_PER: integer := 105;
constant LIM_PER1 : integer := 99;
constant LIM_PER2 : integer := 95;
constant LIM_PER3 : integer := 90;
constant LIM_PER4 : integer := 80;
constant LIM_PER5 : integer := 70;
constant LIM_PER6 : integer := 60;
constant LIM_PER7 : integer := 50;

-- Update period
constant UPDATE_COUNT : integer := update_period_ms * clk_freq_mhz;

-- Expected number of frames in update period
constant FRAME_CNT_MAX : integer := UPDATE_COUNT / frame_count_expected;

constant ERROR_CNT: integer := (ERROR_PER * FRAME_CNT_MAX)/100;
constant LIM_CNT1 : integer := (LIM_PER1 * FRAME_CNT_MAX)/100;
constant LIM_CNT2 : integer := (LIM_PER2 * FRAME_CNT_MAX)/100;
constant LIM_CNT3 : integer := (LIM_PER3 * FRAME_CNT_MAX)/100;
constant LIM_CNT4 : integer := (LIM_PER4 * FRAME_CNT_MAX)/100;
constant LIM_CNT5 : integer := (LIM_PER5 * FRAME_CNT_MAX)/100;
constant LIM_CNT6 : integer := (LIM_PER6 * FRAME_CNT_MAX)/100;
constant LIM_CNT7 : integer := (LIM_PER7 * FRAME_CNT_MAX)/100;

signal frame_count, cnt : integer := 0;
signal reset_cnt : std_logic := '0';

begin
	error_flag <= '1' when frame_count > ERROR_CNT else '0';
	
	p_update : process(rst, clk)
	begin
		if rst = '1' then
			reset_cnt <= '1';
			cnt <= UPDATE_COUNT - 1;
			dataout <= "000";
		elsif rising_edge(clk) then
			-- update the period
			if cnt = 0 then
				cnt <= UPDATE_COUNT - 1;
				reset_cnt <= '1';
			else
				cnt <= cnt - 1;
				reset_cnt <= '0';
			end if;
			
			if cnt = 0 then
				if frame_count > LIM_CNT1 then
					dataout <= "000"; --  99
				elsif frame_count > LIM_CNT2 then
					dataout <= "001"; -- 95		
				elsif frame_count > LIM_CNT3 then
					dataout <= "010";	-- 90			
				elsif frame_count > LIM_CNT4 then
					dataout <= "011";-- 80
				elsif frame_count > LIM_CNT5 then
					dataout <= "100";	-- 70			
				elsif frame_count > LIM_CNT6 then
					dataout <= "101";	-- 60			
				elsif frame_count > LIM_CNT7 then
					dataout <= "110"; -- 50
				else
					dataout <= "111";
				end if;			
			end if;
			
		end if;
	end process;

	p_frame_cnt : process(rst, fp)
	begin
	if rst = '1' or reset_cnt='1' then
		frame_count <= 0;
	elsif rising_edge(fp) then
		frame_count <= frame_count + 1;
	end if;
	
	end process;

end rtl;
				