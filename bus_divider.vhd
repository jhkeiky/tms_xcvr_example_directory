library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bus_divider is
generic
(
	div_parameter : natural := 1 -- Sets the divider by 2**N
);
port 
( 
	rst : in std_logic;

	div_in : in std_logic_vector(7 downto 0);
	div_out: out std_logic_vector(7 downto 0) := (others => '0')
);
end bus_divider;

architecture rtl of bus_divider is
begin

	g_div : for index in div_in'range generate
	begin
		div_comp_i : entity work.clockdiv
		generic map 
		(
			N => div_parameter
		)
		port map
		( 
		   RST => rst,
			CLK => div_in(index),
			OCLK => div_out(index)
		);
	end generate;
end rtl;