library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Ensures that the data is at least varying from one
-- received word to the next.
entity rx_data_check is
generic
(
	rx_data_width : integer := 16
);
port
(
	rst : in std_logic;
	rx_clk : in std_logic;
	rx_parallel_datain : in std_logic_vector(rx_data_width-1 downto 0);
	check_output : out std_logic
);
end rx_data_check;

architecture arch of rx_data_check is

signal rx_parallel_datain_p : std_logic_vector(rx_parallel_datain'range) :=  (others => '0');
signal y : std_logic := '0';

begin
	check_output <= y;

	process(rst, rx_clk)
	begin
		if rst = '1' then
			rx_parallel_datain_p <= (others => '0');
			y <= '0';
		elsif rising_edge(rx_clk) then
			rx_parallel_datain_p <= rx_parallel_datain;

			if rx_parallel_datain_p /= rx_parallel_datain then
				y <= not y;
			end if;
		end if;
	end process;
end;