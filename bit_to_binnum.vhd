library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bit_to_binnum is
	port (
		datain	: in std_logic_vector(7 downto 0);
		patterndetect : out std_logic;
		errordetect : out std_logic
	);
end entity bit_to_binnum;

-- Takes in the width 8 data bus from the stm_data_align_wrapper and turns the position into a binary number
architecture arch of bit_to_binnum is
constant ZERO : std_logic_vector(datain'range) := (others => '0');
begin
	patterndetect <= '1' when datain /= ZERO else '0';
	process(datain)
	begin
		case datain is
			when "10000000" =>
				errordetect <= '0';
			when "01000000" =>
				errordetect <= '0';
			when "00100000" =>
				errordetect <= '0';
			when "00010000" =>
				errordetect <= '0';
			when "00001000" =>
				errordetect <= '0';
			when "00000100" =>
				errordetect <= '0';
			when "00000010" =>
				errordetect <= '0';
			when "00000001" =>
				errordetect <= '0';
			when "00000000" =>
				errordetect <= '0';
			when others =>
				errordetect <= '1';

		end case;
	end process;
end arch;