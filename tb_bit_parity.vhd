library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

library work;
use work.seatechnik_dfo_ssl_oc3pos_pkg.all;

entity tb_bit_parity is
end;

architecture only of tb_bit_parity is
constant WIDTH : natural := 8;

component bit_parity
generic(
      BLOCKSIZE     : natural := 81;  -- number of words in a block
      WIDTH         : natural := 8    -- word size
);     
port 
( 
  clk                 : in  std_logic;                                              
  reset               : in  std_logic;
  en                  : in  std_logic;

  data_vld            : in std_logic;
  data_in             : in std_logic_vector(0 to WIDTH-1);

  wr                  : out  std_logic; -- BIP written to output
  bip                 : out std_logic_vector(0 to WIDTH-1) := (others => '1') 
);
end component bit_parity;

signal reset : std_logic := '0';
signal sys_clk : std_logic := '0';

signal frm_tx_vld  : std_logic := '0';
signal frm_tx_data : std_logic_vector(WIDTH-1 downto 0);

signal tx_clk  : std_logic := '0';

 type T_DATA_IN is array (0 to 3) of std_logic_vector(WIDTH-1 downto 0);
 signal C_DATA_IN      : T_DATA_IN :=
                                (
                                 B"0000_0001",
                                 B"0001_0000",
                                 B"1101_0000", 
                                 B"1110_0111"  
                               );

begin

dut : bit_parity
	generic map (BLOCKSIZE => C_DATA_IN'length, WIDTH => WIDTH)
    port map 
    ( 
      clk                    => sys_clk,               -- in    
      reset                  => reset,                 -- in    
	  en                     => '1',
	  data_vld               => frm_tx_vld,
	  data_in                => frm_tx_data,

	  wr                     => open,
	  bip                    => open 
    );


p_clk : process
   begin
   wait for 5 ns; sys_clk <= '1';
   wait for 5 ns; sys_clk <= '0';
end process;

stimulus : process
   begin
   wait for 5 ns; reset  <= '1';
   wait for 4 ns; reset  <= '0';
   wait;
end process stimulus;


p_tx_vld : process(sys_clk)
	variable cnt : natural := 0;
	begin
	if rising_edge(sys_clk) then
	  cnt := (cnt + 1) mod 2;
		
		if cnt = 0 then
			frm_tx_vld <= '1';
		else
			frm_tx_vld <= '0';			
		end if;		
	end if;
end process;

p_data : process(reset, frm_tx_vld)
	variable cnt : natural := 0;
	begin
	if reset = '1' then
		frm_tx_data <= (others => '0');
		cnt := 0;
	elsif rising_edge(frm_tx_vld) then
		cnt := (cnt + 1) mod C_DATA_IN'length;
		
		frm_tx_data <= C_DATA_IN(cnt);
		
		if cnt = C_DATA_IN'high then
			-- create a circular buffer
			C_DATA_IN(0) <= C_DATA_IN(0)(WIDTH - 2 downto 0) & C_DATA_IN(0)(WIDTH - 1);
		end if;
	end if;
end process;


end only;