library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity frameloss_concatenator is
port
	(
		frameloss_datain : in std_logic_vector(5 downto 0);
		frameloss_errorin: in std_logic_vector(1 downto 0);
		dataout			  : out std_logic_vector(7 downto 0)
	);
end frameloss_concatenator;
	
architecture rtl of frameloss_concatenator is
begin
	dataout <= frameloss_errorin & frameloss_datain;
end rtl;
