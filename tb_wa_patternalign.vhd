library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_wa_patternalign is
end tb_wa_patternalign;

architecture arch of tb_wa_patternalign is
	-- testbench signals
	constant clock_period: time := 40 ps;
	constant tb_data_width: integer := 16;

	signal tb_rst: std_logic := '0';
	signal tb_clk: std_logic := '0';
	signal tb_patterncheck_output: std_logic;
	signal tb_data_output: std_logic_vector(15 downto 0) := (others => '0');

	-- test vector
	type testvector is array (6 downto 0) of std_logic_vector(15 downto 0);  		-- typedef test vector
	signal testvector_inst : testvector := (
							x"0000",
							x"F628",
							x"56A3",
							x"47BF",
							x"6287",
							x"F412",
							x"0000"
						);    						-- instantiate test vector
	signal tb_datain: std_logic_vector(15 downto 0) := testvector_inst(6);			-- connects input to component and test vector

	-- Declare wa_patternalign
	component wa_patternalign is
	generic
	(
		rx_data_width : integer
	);
	port
	(
		rst : in std_logic;
		clk : in std_logic;
		datain : in std_logic_vector(rx_data_width-1 downto 0);
		dataout : out std_logic_vector(rx_data_width-1 downto 0);
		patterndetect : out std_logic
	);
	end component;

begin
	-- instantiate wa_patternalign
	test_wa_patternalign: wa_patternalign
	generic map(rx_data_width => tb_data_width)
	port map(
	  		rst => tb_rst,
			clk => tb_clk,
			datain => tb_datain,
			patterndetect => tb_patterncheck_output,
			dataout => tb_data_output
		);

	-- process for generating the clock
	clock : process
   		begin
			tb_clk  <= not tb_clk;
   			wait for 10 ns; 
	end process clock;

	-- process for incrementing the data feed
	index_decrementer : process(tb_clk)
		variable testvector_index : integer := 6;							-- indexes through the test vector

		begin
		if rising_edge(tb_clk) then
			if testvector_index > 0 then
				testvector_index := testvector_index - 1;
			else
				testvector_index := 6;
			end if;

			tb_datain <= testvector_inst(testvector_index);
		end if;
	end process index_decrementer;
end;
