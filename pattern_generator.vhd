library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;
			
entity pattern_generator is
generic (
		STM  : stm_type := stm1
);
port(
		rst : in std_logic;
		clk : in std_logic;
		
		dataout : out std_logic_vector(7 downto 0)
);
end pattern_generator;
	
architecture rtl of pattern_generator is
	constant STS : natural := conv(STM);
	constant A1_END_CNT : natural := STS;
	constant A2_END_CNT : natural := 2 * STS;

	signal col : integer := 0; -- current column index
	
begin
	process(rst, clk)
	begin
		col <= col + 1;
	
		if col < A1_END_CNT then
			dataout <= STS_FRAME_ALIGNMENT_BYTE_A1;
		elsif col < A2_END_CNT then
			dataout <= STS_FRAME_ALIGNMENT_BYTE_A2;
		else
			col <= 0;
		end if;
	end process;
end rtl;