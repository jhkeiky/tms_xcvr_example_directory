 --=========================================================================
--  This document is the property of KEIKY. The document (or any         --
--  part of it) must not be copied without permission from KEIKY.        --
--  Any authorised copies of this document must include this header.     --
--                (c) Copyright KEIKY Ltd 2018                           --
--=========================================================================
-- File        : bit_parity.vhd
--  
-- Author      : Nicholas St. Hill
-- 
-- Description : This component is used to determine the bit interleave parity
--               (BIP).
--
--=========================================================================
-- Omissions, Limitations, Outstanding Issues :
--
-- None.
--=========================================================================
                  
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity bit_parity is
generic(
      BLOCKSIZE     : integer := 81;  -- number of words in a block
      WIDTH         : natural := 8    -- word size
);     
port 
( 
  clk                 : in  std_logic;                                              
  reset               : in  std_logic;
  en                  : in  std_logic := '1';
  frame_start         : in  std_logic;

  data_vld            : in std_logic;
  data_in             : in std_logic_vector(0 to WIDTH-1);

  wr                  : out  std_logic; -- BIP written to output
  bip                 : out std_logic_vector(0 to WIDTH-1) := (others => '1') 
);
end bit_parity;

architecture rtl of bit_parity is
constant WORD_CNT_MAX : integer := BLOCKSIZE - 1;
signal tmp : std_logic_vector(bip'range) := (others => '0');
signal wr_d, wr_d2 : std_logic := '0';

signal word_cnt : integer := 0; -- number of words in the block
begin

  process(clk,reset)
  begin
    if reset='1' or en='0' then
     word_cnt   <= 0;
	 wr         <= '0'; 
	 wr_d <= '0';
	 wr_d2 <= '0';
     bip        <= (others => '1');
	 tmp        <= (others => '0'); 

    elsif frame_start='0' then
	  
	  if (data_vld = '1') then
		word_cnt <= 1;
		tmp <= data_in;
	  end if;
	  
    elsif falling_edge(clk) then
	  wr_d <= '0';
	  wr_d2 <= wr_d;
	  wr <= wr_d2; -- delay introduced to ensure the data is available before setting the write flag
	  
	  if (data_vld = '1') then
		  word_cnt  <= (word_cnt+1) mod BLOCKSIZE;

		  if (word_cnt = 0) then
			tmp <= data_in;
		  else
			tmp <= tmp xor data_in;	  
		  end if;
		  
		  if word_cnt = WORD_CNT_MAX then
			-- update the BIP
			bip <= tmp xor data_in;
			wr_d <= '1';
		  end if;
	  end if;
    end if;
  end process;
 
 end rtl;  
