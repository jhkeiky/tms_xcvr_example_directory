library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity tb_txdata_sim is
end tb_txdata_sim;

architecture arch of tb_txdata_sim is
	-- testbench signals
	constant clock_period: time := 40 ps;
	constant tb_data_width: integer := 16;

	signal tb_rst: std_logic := '0';
	signal tb_clk: std_logic := '0';
	signal tb_output: std_logic_vector(15 downto 0) := (others => '0');

	-- Declare wa_patternalign
	component xcr_txdata_sim is
	port 
	( 
	  clk           	: in    std_logic; -- twice the desired update rate
	  reset         	: in    std_logic;
	  tx_parallel_data  : out   std_logic_vector(15 downto 0);
	  tx_clkout			: out 	std_logic
	);
	end component;

begin
	-- instantiate xcr_txdata_sim
	test_sim : xcr_txdata_sim
	port map(
	  		reset => tb_rst,
			clk => tb_clk,
			tx_parallel_data => tb_output,
			tx_clkout => open
		);

	-- process for generating the clock
	clock : process
   		begin
			tb_clk  <= not tb_clk;
   			wait for 10 ns; 
	end process clock;
end;