library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity xcvr_txdata_sim is
port 
( 
  clk           	: in    std_logic; -- twice the desired update rate
  reset         	: in    std_logic;
  sel               : in    std_logic := '0';
  tx_parallel_data  : out   std_logic_vector(7 downto 0);
  tx_clkout			: out 	std_logic
);
end xcvr_txdata_sim;

architecture rtl of xcvr_txdata_sim is
 

 
begin
	tx_parallel_data <= x"f6" when sel='0' else x"28";
	tx_clkout <= clk;
end rtl;
