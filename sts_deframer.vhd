--=========================================================================
--  This document is the property of KEIKY. The document (or any         --
--  part of it) must not be copied without permission from KEIKY.        --
--  Any authorised copies of this document must include this header.     --
--                (c) Copyright KEIKY Ltd 2018                           --
--=========================================================================
-- File        : sts_deframer.vhd
--  
-- Author      : Nicholas St. Hill
-- 
--==========================================================================
-- Notes :
-- This component is used to output the data received via and STS-3 message.
-- The component only finds the starting byte sequence so alignment 
-- should be performed external to this component.
-- 
-- Audio data output:
-- The rx_audio_data_out signal is used to ouput all of the header bytes
-- used in the Trelleborg system to output voice.
-- It is the responsibility of components to read the appropriate bytes.
-- for each channel. 
-- The audio channels are arranged with audio channel N at location 
-- rx_audio_data_out(8N+7 downto 8N); where N is zero indexed.
--
-- Bit Interleaved Parity (B1)
-- The even BIP is determined for comparison to the value stored in the last frame.
--
-- TODO:
-- * Verify that all the message bytes are read
-- * Ensure the audio channels are wriiten to the correct addresses
--
--
--==========================================================================
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity sts_deframer is
	generic (
		STM  : stm_type := stm1
	);
	port (
		reset					: in std_logic := '0';
		clk 					: in std_logic := 'X';
		rx_clkin				: in std_logic;
		rx_parallel_data_in 	: in std_logic_vector(7 downto 0);
		rx_payload_data_out		: out std_logic_vector(7 downto 0); -- Payload data received
		rx_payload_clkout		: out std_logic;					-- Clock to read payload data
		rx_telemetry_out		: out std_logic_vector( (8 * conv(STM) * STS_HEADER_BYTES_PER_ROW) - 1 downto 0);
		rx_audio_data_out		: out std_logic_vector( (8 * TMS_NUM_AUDIO_BYTES) - 1 downto 0);
		rx_pattern_detect		: out std_logic;
		rx_bip_valid_flag		: out std_logic;
		rx_concat_detect		: out std_logic				-- Reports detection of concatenation bytes
	);
end entity sts_deframer;

architecture rtl of sts_deframer is
	constant STS : natural := conv(STM);
	constant WIDTH : natural := rx_parallel_data_in'length;
	
	constant TELEMETRY_ROW_INDEX : integer := 2;
	constant AUDIO_ROW_START_INDEX : integer := 5;
	constant B1_ROW_INDEX : integer := 1;
	constant B1_COL_INDEX : integer := 0;
	
	-- The pattern align start count is delayed by 1 because we latch
	-- the data into a buffer for comparison to the alignment pattern
	constant PATTERN_ALIGN_START_CNT : integer := STS + 1;
	constant FRAME_BYTES_PER_ROW : natural := STS * STS_BYTES_PER_ROW;
	constant HEADER_BYTES_PER_ROW : natural := STS * STS_HEADER_BYTES_PER_ROW;
	
	constant MAX_ROW_INDEX : natural := STS_ROWS_PER_FRAME - 1;
	constant MAX_COL_INDEX : natural := FRAME_BYTES_PER_ROW - 1;
	constant MAX_HEADER_COL_INDEX : natural := HEADER_BYTES_PER_ROW	- 1;
	constant MAX_BYTE_PER_FRAME : natural := STS * STS_ROWS_PER_FRAME * STS_BYTES_PER_ROW;
	constant MAX_BYTE_PER_FRAME_INDEX : natural := MAX_BYTE_PER_FRAME - 1;
	
	signal row : integer := 0; -- current row index
	signal col : integer := 0; -- current column index

	signal frame_byte_cnt : integer := 0;
	
	signal rx_data_valid : std_logic := '0';
		
	type T_RD_STATE is (IDLE, RD_HEAD, RD_PAYLOAD);
	signal rd_state   : T_RD_STATE := IDLE;

	-- Signals to store the audio data from the two header rows
	signal audio1, audio2 : std_logic_vector( (WIDTH * HEADER_BYTES_PER_ROW) - 1 downto 0) := (others => '0');
	signal telemetry : std_logic_vector(rx_telemetry_out'range) := (others => '0');
	
	signal pattern_detect : std_logic := '0';
	
	signal alignment_data_buffer : std_logic_vector( (2 * STS_FRAME_ALIGNMENT_BYTE_A1'length) - 1 downto 0) := (others => '0');
	constant alignment_pattern : std_logic_vector(alignment_data_buffer'range) := STS_FRAME_ALIGNMENT_BYTE_A1 & STS_FRAME_ALIGNMENT_BYTE_A2;

	signal rx_payload_flag : std_logic := '0';
	
	signal data_in, data_out 	: std_logic_vector(rx_parallel_data_in'range);
	signal parallel_data_d : std_logic_vector(rx_parallel_data_in'range);
	
	signal enable_scrambler : std_logic := '0'; -- scrambler enable signal
	
	constant RX_DATA_BUFFER_LEN : natural := (STS * STS_FRAME_ALIGNMENT_BYTE_A1'length);
	signal rx_data_buffer : std_logic_vector(RX_DATA_BUFFER_LEN - 1 downto  0) := (others => '0');
	
	signal rx_clk_inv : std_logic;
	
	signal bip_rd, bip_en, bip_data_vld : std_logic;
	
	-- Bit Interleaved Parity for the entire last read frame
	-- This will be compared to the value received in the next frame after 
	-- unscrambling
	signal bip_b1 : std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	
	signal bip_valid_flag : std_logic;
	
	signal data_out_cnt : integer := 0;
	
begin
	rx_clk_inv <= not rx_clkin;
	
	rx_pattern_detect <= pattern_detect;
	rx_concat_detect <= '0';
	
	bip_data_vld <= rx_data_valid;
	bip_en <= rx_data_valid;
	
	rx_bip_valid_flag <= bip_valid_flag;

	scrambler_i : entity work.stm_scrambler
	port map (
		reset	=> reset,
		en		=> enable_scrambler,
		clk 	=> rx_clk_inv,
		din 	=> data_in,
		dout	=> data_out
		);
	
	
	bip_i : entity work.bit_parity
	generic map ( 
		BLOCKSIZE  => MAX_BYTE_PER_FRAME, 
		WIDTH      => WIDTH 
	)     
	port map ( 
	  clk     	=> rx_clkin,                                              
	  reset   	=> reset,
	  en      	=> bip_en,
	  data_vld 	=> bip_data_vld,
	  data_in   => data_out,
	  wr        => bip_rd,
	  bip       => bip_b1 
	);

	
	p_main : process(reset, rx_clkin)
		constant M2 : natural := alignment_data_buffer'length;
		constant N2 : natural := rx_data_buffer'length;
		constant N1 : natural := WIDTH;
	begin
		if reset = '1' then
			pattern_detect <= '0';
			frame_byte_cnt <= 0;
			enable_scrambler <= '0';

			data_in <= (others => '0');
			rx_data_valid <= '0';
			data_out_cnt <= 0;
			row <= 0; col <= 0;
			
		elsif rising_edge(rx_clkin) then
			
			pattern_detect <= '0';
			
			-- Rotate a byte left
			alignment_data_buffer(M2-1 downto WIDTH) <= alignment_data_buffer(M2-WIDTH-1 downto 0);
			alignment_data_buffer(WIDTH-1 downto  0) <= rx_parallel_data_in;
						
			if alignment_data_buffer = alignment_pattern then
				pattern_detect <= '1';				
			end if;

			data_in <= rx_data_buffer(rx_data_buffer'high downto rx_data_buffer'length - WIDTH);
			
			-- Place data in the buffer for delayed output
			rx_data_buffer(N2-1 downto N1) <= rx_data_buffer(N2-N1-1 downto 0);
			rx_data_buffer(N1-1 downto  0) <= alignment_data_buffer(alignment_data_buffer'high downto alignment_data_buffer'length - WIDTH);
						
			if pattern_detect = '1' then
				enable_scrambler <= '0';
				frame_byte_cnt <= 0;	
				row <= 0;
				col <= 0;
				rx_data_valid <= '1';
				
				data_out_cnt <= MAX_BYTE_PER_FRAME;-- reset byte count
			else
			
				if frame_byte_cnt < MAX_BYTE_PER_FRAME_INDEX then
					frame_byte_cnt <= frame_byte_cnt + 1;
				else
					rx_data_valid <= '0';	
				end if;
				
				if rx_data_valid = '1' and data_out_cnt /= 0 then
					data_out_cnt <= data_out_cnt - 1;
				end if;

				-- Enable/disable the scrambler using the scrambler reset flag
				if frame_byte_cnt > (MAX_HEADER_COL_INDEX-1) then
					enable_scrambler <= '1';
				end if;

				if col = MAX_COL_INDEX then -- increment row count			
					if frame_byte_cnt = 0 then
						row <= 0;
					elsif row < MAX_ROW_INDEX then
						row <= row + 1;
					end if;			
				end if;
				
				
				col <= (col + 1) mod FRAME_BYTES_PER_ROW;
				
			end if;
			
			

		end if;
	
	end process;
	
	p_read : process(reset, rx_clkin)
	begin
		if reset = '1' then
			telemetry <= (others => '0');
			audio1 <= (others => '0');
			audio2 <= (others => '0');
			bip_valid_flag <= '0';
		elsif rising_edge(rx_clkin) and rx_data_valid = '1' then
		
			for i in 0 to STS_HEADER_BYTES_PER_ROW - 1 loop
				-- Get the telemetry data
				if row = TELEMETRY_ROW_INDEX and col = i then
					telemetry((i*WIDTH) + (WIDTH-1) downto (i*WIDTH)) <= data_out;
				end if;
				-- Get the first audio row
				if row = AUDIO_ROW_START_INDEX and col = i then
					audio1((i*WIDTH) + (WIDTH-1) downto (i*WIDTH)) <= data_out;
				end if;
				-- Get the second audio row
				if row = (AUDIO_ROW_START_INDEX +1) and col = i then
					audio2((i*WIDTH) + (WIDTH-1) downto (i*WIDTH)) <= data_out;
				end if;
			end loop;
			
			
			-- Read the BIP and compare
			if row = B1_ROW_INDEX and col = B1_COL_INDEX then
				-- This check is only performed once when the data is read
				if data_out = bip_b1 then
					bip_valid_flag <= '1';
				else
					bip_valid_flag <= '0';
				end if;
			end if;

		end if;
	
	end process;
	
		
	p_payload : process(reset, rx_clkin)
	begin
		if reset = '1' or  rx_data_valid = '0' then
			rx_payload_flag <= '0';
		elsif falling_edge(rx_clkin) then
		
			-- set the payload flag
			if col < HEADER_BYTES_PER_ROW then
				rx_payload_flag <= '0';
			else
				rx_payload_flag <= '1';
			end if;

		end if;
	
	end process;

		
	-- Connect the telemetry signal to the output
	rx_telemetry_out <= telemetry;
	
	-- map the audio output signals
	rx_audio_data_out(audio1'range) <= audio1;
	rx_audio_data_out(rx_audio_data_out'high downto audio1'length) <= audio2(rx_audio_data_out'high - audio1'length downto 0);
	
	-- Output the payload
	rx_payload_clkout <= rx_clkin when rx_payload_flag = '1' else '0';
	rx_payload_data_out <= data_out when rx_payload_flag = '1' else (others => '0');


end rtl;

