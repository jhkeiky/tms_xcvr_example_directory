--=========================================================================
--  This document is the property of KEIKY. The document (or any         --
--  part of it) must not be copied without permission from KEIKY.        --
--  Any authorised copies of this document must include this header.     --
--                (c) Copyright KEIKY Ltd 2019                          --
--=========================================================================
-- File        : sts_framer.vhd
--  
-- Author      : Nicholas St. Hill
-- 
-- TODO:
-- * The J0 byte needs to be verified. This can be done when we 
-- connect to an old system and output the received value after 
-- unscrambling.
--
-- * Implement the H1 and H2 overhead.
-- 
-- * Add the audio data to the header
-- 
-- * Insert the payload
--==========================================================================
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity sts_framer is
	generic (
		STM  : stm_type := stm1;
		dataout_word_num    : natural range 1 to 2 := 1
	);
	port (
		reset					: in std_logic := '0';
		clk 					: in std_logic := 'X';
		tx_payload_data			: in std_logic_vector(7 downto 0); -- Payload data received
		tx_payload_clk			: in std_logic; -- Payload clock
		tx_telemetry_data		: in std_logic_vector( (8 * TMS_NUM_TELEMETRY_BYTES) - 1 downto 0);
		tx_telemetry_clk		: in std_logic;
		tx_audio_data			: in std_logic_vector( (8 * TMS_NUM_AUDIO_BYTES) - 1 downto 0);
		tx_audio_clk 			: in std_logic; 
		tx_parallel_data 		: out std_logic_vector((dataout_word_num*8)-1 downto 0);
		tx_clkout				: out std_logic;	-- Clock to read payload data
		tx_byterev_enable		: in std_logic := '0';
		fp						: out std_logic -- Frame Pulse
	);
end entity sts_framer;

architecture rtl of sts_framer is
	constant STS : natural := conv(STM);
	constant WIDTH : natural := 8;
	constant NUM_AUDIO_CH : natural := tx_audio_data'length / 8;
	
	constant A1_END_CNT : natural := STS;
	constant A2_END_CNT : natural := 2 * STS;
	
	
	constant TELEMETRY_ROW_INDEX : integer := 2;
	constant AUDIO_ROW_START_INDEX : integer := 5;
	constant B1_ROW_INDEX : integer := 1;
	constant B1_COL_INDEX : integer := 0;
	
	-- The pattern align start count is delayed by 1 because we latch
	-- the data into a buffer for comparison to the alignment pattern
	constant PATTERN_ALIGN_START_CNT : integer := STS + 1;
	constant FRAME_BYTES_PER_ROW : natural := STS * STS_BYTES_PER_ROW;
	constant HEADER_BYTES_PER_ROW : natural := STS * STS_HEADER_BYTES_PER_ROW;
	
	constant MAX_ROW_INDEX : natural := STS_ROWS_PER_FRAME - 1;
	constant MAX_COL_INDEX : natural := FRAME_BYTES_PER_ROW - 1;
	constant MAX_HEADER_COL_INDEX : natural := HEADER_BYTES_PER_ROW	- 1;
	constant MAX_BYTE_PER_FRAME : natural := STS * STS_ROWS_PER_FRAME * STS_BYTES_PER_ROW;
	constant MAX_BYTE_PER_FRAME_INDEX : natural := MAX_BYTE_PER_FRAME - 1;
		
	signal row : integer range 0 to STS_ROWS_PER_FRAME-1 := 0; -- current row index
	signal col : integer := 0; -- current column index

	signal frame_byte_cnt, payload_byte_cnt : integer := 0;
	
	signal payload_byte_start : integer := 0;--3 * (FRAME_BYTES_PER_ROW - HEADER_BYTES_PER_ROW);
	
	signal wr_header : std_logic := '0'; 

	signal enable_scrambler : std_logic := '0'; -- scrambler enable signal

	signal tx_clk, tx_clk_inv : std_logic:= '0';
	signal bip_rd : std_logic;
	
	-- Bit Interleaved Parity for the entire last read frame
	-- This will be compared to the value received in the next frame after 
	-- unscrambling
	signal bip_b1, bip, bip_tmp : std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	
	signal bip_valid_flag : std_logic;
	
	signal data_in, data_out 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal header_data, payload_data : std_logic_vector(WIDTH-1 downto 0) := (others => '0');
		
	signal output_cnt : natural range 0 to dataout_word_num := 0;
	signal output_word : std_logic_vector(tx_parallel_data'range) := (others => '0');
	signal data_out_d : std_logic_vector(data_out'range);
	
	signal frame_start : std_logic := '0';
	signal s_streami, s_streamo : std_logic_vector(3 downto 0) := (others => '0');
	
	type T_AUDIO_BUS is array (natural range <>) of std_logic_vector (WIDTH-1 downto 0); 
    signal audio_bus, audio_latched : T_AUDIO_BUS(0 to NUM_AUDIO_CH-1)  := (others => (others => '0'));
	signal audio_ch_cnt : natural := 0;
	
begin
	tx_clk <= clk;
	tx_clk_inv <= not clk;
	
	data_in <= header_data when wr_header='1' else payload_data;
	
	-- map the audio input to an internal bus
	G_AUDIO : for i in audio_bus'range generate
		constant N1 : natural := i * audio_bus(0)'length;
		constant N2 : natural := N1 + audio_bus(0)'high;
	begin
		audio_bus(i) <= tx_audio_data(N2 downto N1);
	end generate;
	
	p_main : process(reset, clk)	
	begin
		if reset='1' then
			frame_byte_cnt <= 0;
			row <= 0;
			col	<= 0;
			header_data <= (others => '0');
			payload_data <= (others => '0');
			enable_scrambler <= '0';
			frame_start <= '1';
			s_streami(0) <= '0';
			fp <= '0';
			
		elsif rising_edge(clk) then
			fp <= frame_start;
			-- Delay the frame start input to the scrambler
			s_streami(0) <= frame_start;

			-- Update the key counts
			if frame_byte_cnt = MAX_BYTE_PER_FRAME_INDEX then
				frame_start <= '1';
				frame_byte_cnt <= 0;
				row <= 0;
				col	<= 0;			
			else
				frame_start <= '0';
				frame_byte_cnt <= frame_byte_cnt + 1;
				col <= (col + 1) mod FRAME_BYTES_PER_ROW;
				
				if col = MAX_COL_INDEX then 
					row <= row + 1;
				end if;
			end if;
			
			if col < HEADER_BYTES_PER_ROW then
				wr_header <= '1';
			else
				wr_header <= '0';
			end if;
			
			-- Update the payload count
			if frame_byte_cnt = MAX_HEADER_COL_INDEX then
				payload_byte_cnt <= 0;
			elsif col >= MAX_HEADER_COL_INDEX then
				payload_byte_cnt <= payload_byte_cnt + 1;
			end if;
			
			-- Update the payload data
			if payload_byte_cnt >= payload_byte_start then
				if payload_byte_cnt = payload_byte_start then
					payload_data <= (others => '0');
				else
					payload_data <= std_logic_vector(unsigned(payload_data) + 1);
				end if;
			else
				payload_data <= X"FF"; -- insert in payload area before data start
			end if;
			
			if frame_byte_cnt < A2_END_CNT then
				enable_scrambler <= '0';
			else
				enable_scrambler <= '1';
			end if;

			-- Write the header	data
			if col < HEADER_BYTES_PER_ROW then
			  case row is 
			  when 0 => -- header row 0
				-- Initialize the audio channel count
				audio_ch_cnt <= 0;
				
				if col < A1_END_CNT then
					header_data <= STS_FRAME_ALIGNMENT_BYTE_A1;
				elsif col < A2_END_CNT then
					header_data <= STS_FRAME_ALIGNMENT_BYTE_A2;
				elsif col = A2_END_CNT then
					-- we need to verify that the J0 byte is set correctly
					header_data <= x"01";
				else
					header_data <= (others =>'0');
				end if;

			  when 1 => -- header row 1
				if col = 0 then
					header_data <= bip_b1;
				else
					header_data <= (others =>'0');
				end if;

			  when 5 => -- first audio row
				if audio_ch_cnt < audio_bus'length then
					audio_ch_cnt <= audio_ch_cnt + 1;
					header_data <= audio_latched(audio_ch_cnt);
				end if;
			  when 6 => -- second audio row 
				if audio_ch_cnt < audio_bus'length then
					audio_ch_cnt <= audio_ch_cnt + 1;
					header_data <= audio_latched(audio_ch_cnt);
				end if;
			  when others =>
			  -- do nothing
			  end case;
			
			end if;
				
		end if;	
	end process;
	
	-- process create output word
	p_out : process(reset, clk)
	begin
		if reset='1' then
			output_cnt <= 0;
			output_word <= (others => '0');
			tx_parallel_data <= (others => '0');
			tx_clkout <= '0';
			data_out_d <= (others => '0');
		elsif rising_edge(clk) then
			data_out_d <= data_out;
			
			output_cnt <= (output_cnt + 1) mod dataout_word_num;
			
			if tx_byterev_enable = '0' then
				output_word <= output_word(output_word'high - WIDTH downto 0) & data_out_d;
			else
				output_word <= data_out_d & output_word(output_word'high downto WIDTH);			
			end if;
			
			if output_cnt = 0 then
				-- latch the output
				tx_parallel_data <= output_word;
				tx_clkout <= '1';
			else
				tx_clkout <= '0';
			end if;
		end if;	
	
	end process;

	
	-- process create output word
	p_bip : process(reset, clk, bip)
	begin
		if reset='1' then
			bip_b1 <= (others => '0');
		elsif bip_rd='1' then
			bip_tmp <= bip;
		elsif rising_edge(clk) then
			bip_b1 <= bip_tmp;
		end if;
	end process;

	p_audio : process(reset, tx_audio_clk)
	begin
		if reset='1' then
			audio_latched <= (others => (others => '0'));
		elsif rising_edge(tx_audio_clk) then
			audio_latched <= audio_bus;
		end if;
	end process;

	scrambler_i : entity work.stm_scrambler
	port map (
		reset	=> reset,
		en		=> enable_scrambler,
		clk 	=> clk,
		din 	=> data_in,
		streami => s_streami,
		dout	=> data_out,
		streamo => s_streamo
		);
	
	
	bip_i : entity work.bit_parity
	generic map ( 
		BLOCKSIZE  => MAX_BYTE_PER_FRAME, 
		WIDTH      => WIDTH 
	)     
	port map ( 
	  clk     	=> clk,                                              
	  reset   	=> reset,
	  en      	=> '1',
	  frame_start => s_streamo(0),
	  data_vld 	=> '1',
	  data_in   => data_out,
	  wr        => bip_rd,
	  bip       => bip 
	);



end rtl;