library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wa_bitslipalign is
generic
(
	rx_data_width : integer := 16
);
port
(
	rst : in std_logic;
	clk : in std_logic;
	datain : in std_logic_vector(rx_data_width-1 downto 0);
	bitslip : in std_logic;
	patterndetect : out std_logic;
	dataout : out std_logic_vector(rx_data_width-1 downto 0)
);
end wa_bitslipalign;

architecture arch of wa_bitslipalign is
	-- Sonet framing bytes
	signal A1A2 : std_logic_vector(15 downto 0) := x"F628";

	-- Data buffer
	signal rx_data_buffer : std_logic_vector((2*rx_data_width)-1 downto 0) :=  (others => '0');

	-- Status flags
	signal patterndetect_flag: std_logic := '0';
	signal byte_match : std_logic_vector(A1A2'range) := (others => '0');
	signal byte_match_location : integer := 0;
	
	-- Bitslip flags
	signal bitslip_prev : std_logic := '0';	-- bitslip latch of value from previous clock cycle, see below for reason
	signal bitslip_read : std_logic := '0'; -- actual read signal for bitslip

	-- Word boundary select
	signal boundary_upper : integer := rx_data_buffer'high;
	signal boundary_lower : integer := rx_data_buffer'high-A1A2'high;
	
begin
	-- Assert patterndetect_flag to output
	patterndetect <= patterndetect_flag;

	-- Bitslip_read is high only when bitslip is high (pressed) and bitslip_prev is low (a new press), stops multiple triggers on one press.
	bitslip_read <= (not bitslip_prev) and bitslip;

	-- Initialize data output
	dataout <= (others => '0');

	-- Sequential block
	process(rst, clk)
	begin
		if rst='1' then
			patterndetect_flag <= '0';
			rx_data_buffer <= (others => '0');
			bitslip_prev <= '0';

		elsif rising_edge(clk) then
			-- Latch current bitslip value, 
			bitslip_prev <= bitslip;

			-- Process buffer
			rx_data_buffer((2*rx_data_width)-1 downto rx_data_width) <= rx_data_buffer(rx_data_width-1 downto 0);
			rx_data_buffer(rx_data_width-1 downto 0) <= datain;

			-- Bitslip protocol
			if bitslip = '1' then
				if boundary_lower = 0 then
					boundary_upper <= rx_data_buffer'high;
					boundary_lower <= rx_data_buffer'high-A1A2'high;
				else
					boundary_upper <= boundary_upper - 1;
					boundary_lower <= boundary_lower - 1;
				end if;
			end if;
			
			if rx_data_buffer(boundary_upper downto boundary_lower) = A1A2 then
				patterndetect_flag <= '1';
			else
				patterndetect_flag <= '0';
			end if;

			dataout <= rx_data_buffer(boundary_upper downto boundary_lower);
		end if;
	end process;
end;
