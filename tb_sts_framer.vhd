library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity tb_sts_framer is
end;

architecture only of tb_sts_framer is
constant WIDTH : natural := 8;
constant STS : natural := 3;
constant MAX_BYTE_PER_FRAME : integer := STS * STS_ROWS_PER_FRAME * STS_BYTES_PER_ROW;

component sts_framer is
	generic (
		STM  : stm_type := stm1;
		dataout_word_num    : natural range 1 to 2 := 1
	);
	port (
		reset					: in std_logic := '0';
		clk 					: in std_logic := 'X';
		tx_payload_data			: in std_logic_vector(7 downto 0); -- Payload data received
		tx_payload_clk			: in std_logic; -- Payload clock
		tx_telemetry_data		: in std_logic_vector( (8 * conv(STM) * STS_HEADER_BYTES_PER_ROW) - 1 downto 0);
		tx_telemetry_clk		: in std_logic;
		tx_audio_data			: in std_logic_vector( (8 * TMS_NUM_AUDIO_BYTES) - 1 downto 0);
		tx_audio_clk			: in std_logic;
		tx_parallel_data 		: out std_logic_vector((dataout_word_num*8)-1 downto 0);
		tx_clkout				: out std_logic;	-- Clock to read payload data
		tx_byterev_enable		: in std_logic := '0';
		fp						: out std_logic --Frame Pulse
	);
end component sts_framer;


signal reset : std_logic := '0';
signal sys_clk : std_logic := '0';

signal audio_data : std_logic_vector( (8 * TMS_NUM_AUDIO_BYTES) - 1 downto 0) := (others => '0');
signal audio_clk : std_logic := '0';

begin

audio_data(7 downto 0) <= x"80";
audio_data(15 downto 8) <= x"40";

dut : sts_framer
	generic map (dataout_word_num => 1)
    port map 
    ( 
		reset					=> reset,
		clk 					=> sys_clk,
		tx_payload_data			=> (others => '0'),
		tx_payload_clk			=> '0',
		tx_telemetry_data		=> (others => '0'),
		tx_telemetry_clk		=> '0',
		tx_audio_data			=> audio_data,
		tx_audio_clk			=> audio_clk,
		tx_parallel_data 		=> open,
		tx_clkout				=> open,
		tx_byterev_enable		=> '0',
		fp						=> open
    );


p_clk : process
   begin
   wait for 5 ns; sys_clk <= '1';
   wait for 5 ns; sys_clk <= '0';
end process;

stimulus : process
   begin
   wait for 5 ns; reset  <= '1';
   wait for 4 ns; reset  <= '0';
   wait;
end process stimulus;

p_audio : process
   begin
   wait for 50 ns; audio_clk <= '1';
   wait for 50 ns; audio_clk <= '0';
end process;


end only;