library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tms_sts_pkg.all;

entity patternalign_ctrl is
	port (
		reset 					: in std_logic := 'X';
		rxclk 					: in std_logic := 'X';
		rx_std_wa_patternalign 	: out std_logic
	);
end entity patternalign_ctrl;

architecture rtl of patternalign_ctrl is


constant BYTE_PER_FRAME : integer := 3 * STS_ROWS_PER_FRAME * STS_BYTES_PER_ROW;
constant MAX_CNT : integer := BYTE_PER_FRAME;

constant HOLD_CNT : integer := 16;
signal cnt : integer := HOLD_CNT;

signal wa_patternalign : std_logic := '0';

begin

wa_patternalign <= '1' when cnt < HOLD_CNT else '0';

rx_std_wa_patternalign <= wa_patternalign;

process(reset, rxclk)
begin
	if reset = '1' then
		cnt <= HOLD_CNT;
	elsif rising_edge(rxclk) then
		if cnt = 0 then
			cnt <= MAX_CNT;
		else
			cnt <= cnt - 1;
		end if;
		
	end if;

end process;


end rtl;
