--=========================================================================
--  This document is the property of KEIKY. The document (or any         --
--  part of it) must not be copied without permission from KEIKY.        --
--  Any authorised copies of this document must include this header.     --
--                (c) Copyright KEIKY Ltd 2018                           --
--=========================================================================
-- File        : stm_scrambler.vhd
--  
-- Author      : Nicholas St. Hill
-- 
--==========================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stm_scrambler is
	port (
		reset		: in  std_logic := '0';
		clk 		: in  std_logic := 'X';
		en			: in  std_logic := '1';
		din 		: in  std_logic_vector(7 downto 0);
		streami     : in  std_logic_vector(3 downto 0);
		dout		: out std_logic_vector(7 downto 0);
		streamo     : out std_logic_vector(3 downto 0)
		);
end entity stm_scrambler;

architecture rtl of stm_scrambler is

	constant SEED : std_logic_vector(din'range) := (others => '1');
	signal ifsr : std_logic_vector(din'range) := SEED;
	
	
begin
	
	process(reset, clk)
	begin
		if reset = '1' then
			ifsr <= SEED;
			dout <= (others => '0');
			streamo <= (others => '0');
		elsif rising_edge(clk) then
			streamo <= streami;
			if en = '0' then
				dout <= din;
				ifsr <= SEED;
			else
				-- Latch output
				dout(0) <= din(0) xor ifsr(6);
				dout(1) <= din(1) xor ifsr(6) xor ifsr(5);
				dout(2) <= din(2) xor ifsr(6) xor ifsr(5) xor ifsr(4);
				dout(3) <= din(3) xor ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3);
				dout(4) <= din(4) xor ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2);
				dout(5) <= din(5) xor ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1);
				dout(6) <= din(6) xor ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1) xor ifsr(0);
				dout(7) <= din(7) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1) xor ifsr(0);
			
				-- Update IFSR
				ifsr(0) <= ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1) xor ifsr(0);
				ifsr(1) <= ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1) xor ifsr(0);
				ifsr(2) <= ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1);
				ifsr(3) <= ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3) xor ifsr(2);
				ifsr(4) <= ifsr(6) xor ifsr(5) xor ifsr(4) xor ifsr(3);
				ifsr(5) <= ifsr(6) xor ifsr(5) xor ifsr(4);
				ifsr(6) <= ifsr(6) xor ifsr(4) xor ifsr(3) xor ifsr(2) xor ifsr(1) xor ifsr(0);
			end if;
		end if;
	end process;
	


end rtl;

