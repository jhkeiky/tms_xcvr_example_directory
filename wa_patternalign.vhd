library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wa_patternalign is
generic
(
	rx_data_width : integer := 8
);
port
(
	rst : in std_logic;
	clk : in std_logic;
	datain : in std_logic_vector(rx_data_width-1 downto 0);
	dataout : out std_logic_vector(rx_data_width-1 downto 0);
	patterndetect_a1 : out std_logic;
	patterndetect_a2 : out std_logic
);
end wa_patternalign;

architecture arch of wa_patternalign is

	
begin


	-- Sequential block
	process(rst, clk)
	begin
		if rst='1' then
			patterndetect_a1 <= '0';
			patterndetect_a2 <= '0';

		-- Update output word boundary
		elsif rising_edge(clk) then

			
			if datain = x"f6" then
				patterndetect_a1 <= '1';
			else
				patterndetect_a1 <= '0';
			end if;
			
			if datain = x"28" then
				patterndetect_a2 <= '1';
			else
				patterndetect_a2 <= '0';
			end if;		end if;
	end process;
end;
			