library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package tms_sts_pkg is  
  constant STS_FRAME_ALIGNMENT_BYTE_A1 : std_logic_vector(7 downto 0) := X"F6";
  constant STS_FRAME_ALIGNMENT_BYTE_A2 : std_logic_vector(7 downto 0) := X"28";
  
  constant STS_CONCATENATION_BYTE_H1 : std_logic_vector(7 downto 0) := X"93";
  constant STS_CONCATENATION_BYTE_H2 : std_logic_vector(7 downto 0) := X"FF";
  
  constant STS_FIXED_STUFF_COL1 : integer := 30;
  constant STS_FIXED_STUFF_COL2 : integer := 59;
 
  constant STS_ROWS_PER_FRAME : integer := 9;
  constant STS_BYTES_PER_ROW : integer := 90;
  constant STS_HEADER_BYTES_PER_ROW : integer := 3;
     
  constant TMS_NUM_AUDIO_BYTES : natural := 16;
  constant TMS_NUM_TELEMETRY_BYTES : natural := STS_HEADER_BYTES_PER_ROW;
  
  constant TMS_XCVR_ALIGN_PATTERN: std_logic_vector(15 downto 0) := STS_FRAME_ALIGNMENT_BYTE_A1 & STS_FRAME_ALIGNMENT_BYTE_A2;
  
  type stm_type is (stm1, stm4);
  function conv (a: stm_type) return natural;
 
  --*********************************************
  -- System Wide types and conversion functions *  
  --*********************************************
  
  subtype   nybble          is std_logic_vector (3 downto 0);
  subtype   byte            is std_logic_vector (7 downto 0);
  subtype   word_16         is std_logic_vector (15 downto 0);
  subtype   word_32         is std_logic_vector (31 downto 0);
  subtype   word_64         is std_logic_vector (63 downto 0);

  type      nybble_array  is array (natural range <>) of nybble;
  type      byte_array    is array (natural range <>) of byte;
  type      boolean_array is array (natural range <>) of boolean;
  type      natural_array is array (natural range <>) of natural;
  type      word_16_array is array (natural range <>) of word_16;
  type      word_32_array is array (natural range <>) of word_32;
end tms_sts_pkg;


package body tms_sts_pkg is

  function conv (a: stm_type) return natural is
    variable t : natural; 
  begin
    case a is
      when stm1  => t := 3;
      when stm4  => t := 12;
    end case;
    return t;
  end;

end tms_sts_pkg;
